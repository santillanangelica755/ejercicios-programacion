/*
Programa: PerimetroEquilatero_Programa.c
Autor: Ang�lica Santill�n
Objetivo: Elabore un programa que calcule el per�metro de un tri�ngulo equil�tero
Fecha: 22/10/2022
*/

//Bibliotecas
#include <stdio.h>
#include <locale.h>

/*
	Procedimiento Funci�n perimetro
	Objetivo: Realiza el calculo del perimetro
*/
int perimetro(int lado){
	lado = 3 * lado;
	return lado;
}

int main (){
	int lado = 0;
	
	setlocale(LC_CTYPE, "spanish");
	printf("Programa que calcula el per�metro de un tri�ngulo equil�tero\n");
	printf("Recuerda. Un tri�ngulo equil�tero tiene sus tres lados iguales. \n\n");
	printf("Ingrese la medida de un lado del tri�ngulo: ");
	scanf("%d", &lado);
	
	printf("El valor del per�metro del tri�ngulo equil�tero es: %d", perimetro(lado));
	
	return 0;
}
