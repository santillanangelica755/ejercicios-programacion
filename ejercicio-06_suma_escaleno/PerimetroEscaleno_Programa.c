/*
Programa: PerimetroEscaleno_Programa.c
Autor: Angélica Santillán
Objetivo: Programa que calcule el perímetro de un triángulo escaleno
Fecha: 23/10/2022
*/

//Bibliotecas
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

/*
	Procedimiento Función perimetro
	Objetivo: Realiza el calculo del perimetro
*/

int perimetro(int numeros[3]){
	int perimetro = 0;
	int indice;
	
	for (indice = 0; indice < 3; indice ++){
		perimetro = perimetro + numeros [indice];
	}
	
	return perimetro;
}

int main (){
	
	int numeros[3];
	int indice;
	
	setlocale(LC_CTYPE, "spanish");
	printf("Programa que calcule el perímetro de un triángulo escaleno\n");
	printf("Recuerda. Un triángulo escaleno tiene sus tres lados diferenes. \n\n");
	
	for(indice = 0; indice < 3; indice ++){
		
		printf("Ingrese el lado %d: ", indice +1);
		scanf("%d", & numeros[indice]);
	}

	printf("El valor del perímetro del triángulo escaleno es: %d", perimetro(numeros));
	
	return 0;
}
