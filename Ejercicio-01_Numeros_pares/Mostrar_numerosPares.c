/*
Programa: Ejercicio-01_Numeros_pares
Autor: Ang�lica Santill�n
Objetivo: Elabora un programa que imprima los n�meros pares del 0 al 100.
Fecha: 19/10/2022
*/
//Bibliotecas
#include <stdio.h>
#include <locale.h>

int main(){
	
	setlocale(LC_CTYPE, "spanish");
    int numero;
    int divisor = 2;

    printf("N�meros pares:\n");

    for (numero = 0; numero <=100; numero ++){
        if(numero % divisor == 0){
            printf("%d ", numero);
        }
    }
    return 0;
}
