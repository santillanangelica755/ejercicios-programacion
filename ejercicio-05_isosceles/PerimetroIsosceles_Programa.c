/*
Programa: PerimetroIsosceles_Programa.c
Autor: Angélica Santillán
Objetivo: Programa que calcule el perímetro de un triángulo isósceles
Fecha: 22/10/2022
*/

//Bibliotecas
#include <stdio.h>
#include <locale.h>

/*
	Procedimiento Función perimetro
	Objetivo: Realiza el calculo del perimetro
*/
int perimetro(int ladosIguales, int ladoDesigual){
	int perimetro = 0;
	
	perimetro = (2*ladosIguales) + ladoDesigual;
	return perimetro;
}

int main (){
	int ladosIguales = 0;
	int ladoDesigual = 0;
	
	setlocale(LC_CTYPE, "spanish");
	printf("Programa que calcula el perímetro de un triángulo isósceles\n");
	printf("Recuerda. Un triángulo isósceles tiene dos lados iguales y uno diferente. \n\n");
	printf("Ingrese la medida del lado del triángulo que se repite: ");
	scanf("%d", &ladosIguales);
	printf("Ingrese la medida del lado diferente del triángulo: ");
	scanf("%d", &ladoDesigual);
		
	printf("El valor del perímetro del triángulo isósceles es: %d", perimetro(ladosIguales,ladoDesigual));
	
	return 0;
}
