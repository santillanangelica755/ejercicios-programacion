/*
Programa: dolaresPesos_Programa.c
Autor: Ang�lica Santill�n
Objetivo: Programa que recibe como entrada un monto en d�lares (USD) y devuelve la cantidad
			equivalente en pesos mexicanos (MXN)
Fecha: 19/10/2022
*/

//Bibliotecas
#include <stdio.h>
#include <locale.h>

/*
	Procedimiento Funci�n convertir
	Objetivo: Realizar la conversi�n de dolares a pasos mexicanos
*/
float convertir(float dolares){
	float tipoCambio = 19.92;
	dolares = dolares * tipoCambio;
	return dolares;
}
int main(){
	
	float dolares = 0;
	setlocale(LC_CTYPE, "spanish");
	
	printf("Programa que recibe como entrada un monto en d�lares (USD)\n"
	 "y devuelve la cantidad equivalente en pesos mexicanos (MXN)");
	printf("\n\nIngresa el monto en d�lares: ");
	scanf("%f", &dolares);
	
	printf("El monto ingresado en dolares es igual a %.2f pesos mexicanos", convertir(dolares));
	return 0;
}
