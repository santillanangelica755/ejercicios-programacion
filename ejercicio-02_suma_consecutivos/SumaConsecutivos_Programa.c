/*
Programa: SumaConsecutivos_Programa.c
Autor: Ang�lica Santill�n
Objetivo: Programa que recibe un n�mero entre 1 y 50 y devuelve la suma de los n�meros 
            consecutivos desde 1 hasta ese n�mero
Fecha: 19/10/2022
*/
//Bibliotecas
#include <stdio.h>
#include <locale.h>

//Constantes
#define VERDADERO 1
#define FALSO 0
/*
	Procedimiento Funci�n validarNumero
	Objetivo: Validar que el n�mero se encuentre dentro del rango establecido 1 al 50
*/
int validarNumero(int numero){
	
    if(numero > 50){
        return FALSO;
    }else{
		return VERDADERO;
    }
    
}

/*
	Procedimiento Funci�n sumaNumero
	Objetivo: Realiza la suma de los n�meros consecutivos desde 1 hasta el n�mero que ingreso el usuario. 
*/
int sumaNumero(int numero){
	
	numero = (numero*(numero + 1)) / 2;
	return numero;

}

int main (){
    int numero = 0;
    char mensaje_error[] = "El n�mero ingresado es mayor a 50";
    
    setlocale(LC_CTYPE, "spanish");
    printf("Programa que recibe un n�mero entre 1 y 50 y devuelve la suma de los n�meros\n"
            "consecutivos desde 1 hasta ese n�mero\n");
    
    	printf("Ingresa un n�mero: ");
		scanf("%d", &numero);
		
		if(validarNumero(numero) == 1){	
		    printf("La suma del n�mero es: %d\n", sumaNumero(numero));
		}else{
			printf("%s", mensaje_error);
		}

    return 0;
}
